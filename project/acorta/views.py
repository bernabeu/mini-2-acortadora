from django.shortcuts import render
from django.http import HttpResponse, Http404, HttpResponsePermanentRedirect
from .models import Contenido, Shortvacio
from .forms import FromInfo
from django.template import loader
from django.views.decorators.csrf import csrf_exempt

# Create your views here.

def formato(url, short):
    if not (url.startswith("http://") or url.startswith("https://")):
        url = "http://" + url
        short = 'http://localhost:8000/' + str(short)
    else:
        short = "http://localhost:8000/'" + str(short)
    return url, short

def manageShortvacio(short):
    shortvacio = Shortvacio(short=short)
    shortvacio.save()
    short = shortvacio.id
    return short

def urlNueva(url, urls, short):
    if url in urls:
        contenido = Contenido.objects.get(url=url)
        contenido.short = short
        contenido.save()
    else:
        urlactualizada = Contenido(short=short, url=url)
        urlactualizada.save()

@csrf_exempt
def index(request):
    formulario_vacio = FromInfo()
    if request.method == 'POST':
        respuestas = FromInfo(request.POST)
        if respuestas.is_valid():
            short = respuestas.cleaned_data.get('short')
            url = respuestas.cleaned_data.get('url')
            if short == '':
                short = manageShortvacio(short)
            url, short = formato(url, short)
            urls = Contenido.objects.values_list('url', flat=True)
            urlNueva(url, urls, short)
            respuestas_guardadas = Contenido.objects.all()
            template = loader.get_template('formulario.html')
            contexto = {'respuestas_guardadas': respuestas_guardadas, 'formulario_vacio': formulario_vacio}
            return HttpResponse(template.render(contexto, request))
        else:
            raise Http404('La url introducida no es válida')
    else:
        respuestas_guardadas = Contenido.objects.all()
        context = {'respuestas_guardadas': respuestas_guardadas, 'formulario_vacio': formulario_vacio}
        return render(request, 'formulario.html', context)

@csrf_exempt
def getResource(request, recurso):
    try:
        recurso = f'http://localhost:8000/{recurso}'
        contenido = Contenido.objects.get(short=recurso)
        respuesta = HttpResponsePermanentRedirect(contenido.url)
        return respuesta
    except Contenido.DoesNotExist:
        raise Http404(f'<h1>Recurso no disponible<h1>')



