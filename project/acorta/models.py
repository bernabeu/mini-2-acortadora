from django.db import models

# Create your models here.

class Contenido(models.Model):
    url: str = models.CharField(max_length=64)
    short = models.CharField(max_length=64, blank=True)

class Shortvacio(models.Model):
    short = models.CharField(max_length=64, blank=True)
